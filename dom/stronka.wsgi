#!/usr/bin/env python
# -*- coding=utf-8 -*-

import bookdb

body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Baza Książek :)</title>
    </head>"""

def Spis(environ, start_response):

    database = bookdb.BookDB()
    titles = database.titles()
    response_body = body + "<h1>Spis Książek :</h1> <body>"

    for title in titles:
        response_body += "- <a href='/~p6/wsgi/info=%s'>" %title['id'] + title['title'] + "</a></br></br>"

    response_body += """</body></html>"""

    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]

def Info(environ, start_response, id):

    database = bookdb.BookDB()
    ksiazka = database.title_info(id)
    response_body = body + "<h1>Szczegółowe info :</h1> <body>"

    response_body += "Tytuł : " + ksiazka['title'] + "</br>" \
                     "ISBN : " + ksiazka['isbn'] + "</br>" \
                     "Wydawca : " + ksiazka['publisher'] + "</br>" \
                     "Autor : " + ksiazka['author'] + "</br></br>" \
                     "<a href='/~p6/wsgi/spis'>Powrót</a></br></br>" \

    response_body += """</body></html>"""

    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]

def Blad(environ, start_response):

    response_body = body + "<h1>Książka nie istnieje w bazie</h1> <body><p>404 NOT FOUND :)</p></body></html>"

    status = '404 NOT FOUND'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]

def application(environ, start_response):

    tmp = environ['PATH_INFO']

    if tmp[:17] == "/~p6/wsgi/info=id" :
        try:
            if int(tmp.split('id')[1]) > 0 and int(tmp.split('id')[1]) < 6 :
                 return Info(environ, start_response, tmp.split('=')[1])
            else:
                return Blad(environ, start_response)
        except:
            return Blad(environ, start_response)

    elif tmp == "/~p6/wsgi/spis" or tmp == "/~p6/wsgi":
        return Spis(environ, start_response)

    else:
        return Blad(environ, start_response)

if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('194.29.175.240', 8642, application)
    srv.serve_forever()