#!/usr/bin/env python
# -*- coding=utf-8 -*-


import socket
import unittest
import requests


class tests(unittest.TestCase):


    def GetRequest(self, qs):
        url = 'http://194.29.175.240/~p6/wsgi' + qs
        session = requests.session()
        response = session.get(url)
        session.close()
        return response


    def test_404(self):
        answer = self.GetRequest('/info=dasda')
        expected = True

        toCheck = False
        if '<h1>Książka nie istnieje w bazie</h1> <body><p>404 NOT FOUND :)</p></body>' in answer.content:
            toCheck = True
        self.assertEqual(toCheck ,expected)

    def test_200(self):
        answer = self.GetRequest('/info=id4')
        expected = True

        toCheck = False
        if '<body>Tytuł : Python Cookbook, Second Edition</br>ISBN : 978-0-596-00797-3</br>Wydawca : OReilly Media</br>Autor : Alex Martelli, Anna Ravenscroft, David Ascher</br>' in answer.content:
            toCheck = True
        self.assertEqual(toCheck, expected)

    def test_main_page(self):
        answer = self.GetRequest('')
        expected = True

        toCheck = False
        if '<h1>Spis Książek :</h1>' in answer.content and "<a href='/~p6/wsgi/info=" in answer.content:
            toCheck = True


        self.assertEqual(toCheck, expected)

if __name__ == '__main__':
    unittest.main()